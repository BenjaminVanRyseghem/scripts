# Auto completion
autoload -U compinit
compinit
zstyle ':completion:*' menu select

# Enable colour support for ls
if [ "$TERM" != "dumb" ] && [ -x /usr/bin/dircolors ]
then
  eval "`dircolors -b`" # This is actually code for Bash, but it seems to work well enough
fi

# A few lines here based on http://aperiodic.net/phil/prompt/prompt.txt (http://aperiodic.net/phil/prompt/)
autoload colors zsh/terminfo

if [[ "$terminfo[colors]" -ge 8 ]]
then
  colors
  for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE
  do
    eval PR_$color='%{$terminfo[bold]$fg[${(L)color}]%}'
    eval PR_LIGHT_$color='%{$fg[${(L)color}]%}'
  done
  PR_NO_COLOUR="%{$terminfo[sgr0]%}"

  PROMPT="${PR_GREEN}%n${PR_NO_COLOUR}@${PR_BLUE}%1~${PR_NO_COLOUR}: "
else
  PROMPT="%n@%~: "
fi

# Preferences from ubuntu.fr

zstyle ':completion:*:rm:*' ignore-line yes
zstyle ':completion:*:mv:*' ignore-line yes
zstyle ':completion:*:cp:*' ignore-line yes

zstyle ':completion:*:sudo:*' command-path /usr/local/sbin /usr/local/bin \
                           /usr/sbin /usr/bin /sbin /bin /usr/X11R6/bin

autoload -U bashcompinit
bashcompinit

setopt correctall

# Historic
export HISTSIZE=2000
export HISTFILE="$HOME/.history"
export SAVEHIST=$HISTSIZE
setopt hist_ignore_all_dups

# Extensions

setopt autocd
setopt extendedglob

# Add custom directory to PATH
for dir in \
  /Users/benjamin/.gem/ruby/1.8/bin \
  /opt/local/bin \
  /usr/local/git/bin \
  ~/.cabal/bin \
  /usr/texbin \
  /usr/local/share/npm/bin/ \
  /Users/benjamin/.arcanist/arcanist/bin \
  /Users/benjamin/projects/chromium/depot_tools \
  /Library/TeX/texbin \
; do
  if [[ -d $dir ]]; then export PATH=$PATH:$dir; fi
done

export DYLD_FALLBACK_LIBRARY_PATH=/usr/local/opt/openssl/lib:$DYLD_FALLBACK_LIBRARY_PATH

# Add jump auto completion
source `jump-bin --zsh-integration`

# Set preferences
export EDITOR=mate

source /Users/benjamin/.zshrc_brew

# Alias

alias j=jump
alias gitl="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %G? %C(bold blue)<%an>%Creset' --abbrev-commit"

PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting
PATH=/usr/local/sbin:$PATH
PATH=$HOME/.bin:$PATH
PATH="/usr/local/opt/imagemagick@6/bin:$PATH"
PATH="$HOME/work/sonar-scanner/bin:$PATH"
PATH="$HOME/work/android-sdk/bin:$PATH"

alias fuck='eval $(thefuck $(fc -ln -1))'
# You can use whatever you want as an alias, like for Mondays:
alias FUCK='fuck'

# SandGlass
alias sg='sand-glass'

# Better keys in Terminal

# this makes the "delete" key work rather than 
# just entering a ~
bindkey "\e[3~" delete-char

# these allow you to use ctrl+left/right arrow keys
# to jump the cursor over words
bindkey "\e[1;5C" forward-word
bindkey "\e[1;5D" backward-word

# Plugins

# plugins=(git bundler osx rake ruby)


if [ ! -S ~/.ssh/ssh_auth_sock ]; then
    { eval `ssh-agent 2>/dev/null` ; ssh-add -K .ssh/id_rsa .ssh/github_rsa .ssh/gitlab_id_rsa .ssh/gitlab-cyberzen } &>/dev/null
    ln -sf "$SSH_AUTH_SOCK" ~/.ssh/ssh_auth_sock
fi
export SSH_AUTH_SOCK=~/.ssh/ssh_auth_sock
export GPG_AGENT_INFO="~/.gnupg/S.gpg-agent:$(pgrep gpg-agent):1" # https://github.com/funtoo/keychain/issues/59
eval `keychain --eval --agents ssh,gpg --inherit any id_rsa gitlab_id_rsa github_rsa gitlab-cyberzen 55B9EC85 2> /dev/null`

# rbenv

eval "$(rbenv init -)"

export PATH="$HOME/.yarn/bin:$PATH"
export PATH="$HOME/android-sdk:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

export DEVKITPRO=/opt/devkitpro

  # Set Spaceship ZSH as a prompt
  autoload -U promptinit; promptinit
  SPACESHIP_DIR_PREFIX="%{%b%}%{%B%F{green}%}@%{%b%f%}%{%B%}"
  SPACESHIP_USER_SHOW="always"
  SPACESHIP_USER_COLOR="green"
  SPACESHIP_DIR_COLOR=4
  SPACESHIP_USER_SUFFIX=""
  SPACESHIP_GIT_STATUS_COLOR=14
  SPACESHIP_PACKAGE_COLOR=14
  prompt spaceship
export PATH="/usr/local/opt/gettext/bin:$PATH"
