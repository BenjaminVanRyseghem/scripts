#!/bin/sh

set -e

cd "$(dirname "$0")"

./add-flags-to-app.sh "/Applications/Google Chrome.app" --remote-debugging-port=9223