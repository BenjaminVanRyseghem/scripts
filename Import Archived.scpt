-- Import selected Mail messages & attachments to DEVONthink Pro.
-- Created by Christian Grunenberg on Mon Mar 05 2012.
-- Copyright (c) 2012-2015. All rights reserved.

-- this string is used when the message subject is empty
property pNoSubjectString : "(no subject)"
property dbName : "Mail Archive"

if application "Mail" is running then
	tell application "Mail"
		try
			set mb to mailbox "Archived"
			set theSelection to messages of mb
			set target to mailbox "Archive"
			if the length of theSelection is less than 1 then return
			tell application id "DNtp"
				set db to my findDb(dbName)
				if not (exists db) then return
				set theGroup to incoming group of db
			end tell
			repeat with theMessage in theSelection
				my importMessage(theMessage, theGroup)
				move theMessage to target
			end repeat
		on error error_message number error_number
			if error_number is not -128 then display alert "Mail" message error_message as warning
		end try
	end tell
end if

on importMessage(theMessage, theGroup)
	tell application "Mail"
		try
			tell theMessage
				set {theDateReceived, theDateSent, theSender, theSubject, theSource, theReadFlag} to {the date received, the date sent, the sender, subject, the source, the read status}
			end tell
			if theSubject is equal to "" then set theSubject to pNoSubjectString
			tell application id "DNtp"
				set newRecord to create record with {name:theSubject & ".eml", type:unknown, creation date:theDateSent, modification date:theDateReceived, URL:theSender, source:(theSource as string), unread:(not theReadFlag)} in theGroup
				set finalRecord to convert record newRecord to html
				set newRecordName to name of newRecord
				-- Remove extension
				set finalName to (texts 1 thru -5 of newRecordName)
				set name of finalRecord to finalName
				delete record newRecord
			end tell
		on error error_message number error_number
			if error_number is not -128 then display alert "Mail" message error_message as warning
		end try
	end tell
end importMessage

on findDb(wantedName)
	tell application id "DNtp"
		set dbs to get databases
		repeat with aDb in dbs
			set theName to aDb's name
			if theName is wantedName then return aDb
		end repeat
	end tell
end findDb