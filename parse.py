#!/usr/bin/env python

import parsedatetime
import sys
from time import mktime
from datetime import datetime

AS_DATE_FORMAT = "%d/%m/%y"

string = sys.argv[1]
cal = parsedatetime.Calendar()

(val, code) = cal.parse(string)

# possible values for code
# 1 = date (with current time, as a struct_time)
# 2 = time (with current date, as a struct_time)
# 3 = datetime

dt = datetime.fromtimestamp(mktime(val))
print dt.strftime(AS_DATE_FORMAT)