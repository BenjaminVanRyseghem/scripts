#!/usr/bin/env ruby

# scp "$1" benjamin@benjamin.is-a-geek.com:~/nzb
# scp "$1" benjamin@benjamin.est-le-patron.com:~/nzb

require 'nokogiri'
require 'set'

filename = ARGV[0]
subdir = ""

f = File.open(filename)
doc = Nokogiri::XML(f)
f.close

groups = doc.css("group")
name = doc.at_css("file").attr("subject")

movie = groups.any? {|group|
  group.to_s.include? "movie"
}

movie = groups.any? {|group|
  group.to_s.include? "movie"
}

if movie
  subdir = "/movies"
else
  serie = groups.any? {|group|
    (group.to_s.include? "tv") || (group.to_s.include? "serie")
  }
  if serie
    subdir = "/tv"
  else
    if ((/[0-9]{1,2}[xX][0-9]{2}/).match(name) || (/[sS][0-9]{1,2}[eE][0-9]{2}/).match(name))
      subdir = "/tv"
    else
      anime = groups.any? {|group|
        group.to_s.include? "cartoon"
      }
      if anime
        subdir = "/anime"
      end
    end
  end
end

`scp "#{filename}" "benjamin@benjamin.is-a-geek.com:~/nzb#{subdir}"`
# if ($?.to_i != 0)
#  exit $?.to_i
# end
# 
# `scp "#{filename}" "benjamin@benjamin.est-le-patron.com:~/nzb#{subdir}"`
exit $?.to_i