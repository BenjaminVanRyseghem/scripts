#!/bin/bash
EXPECTED_SSID="Undercover-FBI-Van"

WIFI_OFF=$(networksetup -getairportpower en0 | grep On)
INTERFACE=$(networksetup listallhardwareports | sed -n "/Hardware\ Port:\ Wi-Fi/{n;p;}" | sed  s/Device:\ //)
ALL_SSIDS=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -s -x)
EXPECTED_REACHABLE=$(echo ${ALL_SSIDS} | grep "<string>${EXPECTED_SSID}</string>")
SSID=$(/System/Library/PrivateFrameworks/Apple80211.framework/Versions/Current/Resources/airport -I | egrep "[[:blank:]]SSID" | sed -e "s/[[:blank:]]*SSID:[[:blank:]]//g" )

if [[ -z  $WIFI_OFF  ]]; then
  echo "WiFi if off"
  networksetup -setairportpower "${INTERFACE}" on
  exit 0
fi


if [[ -z  $ALL_SSIDS  ]]; then
  echo "No WiFi reachable"
  exit 0  
fi


if [[ -z  $EXPECTED_REACHABLE  ]]; then
  echo "Expected WiFi not reachable"
  exit 0  
fi

if $(test "$SSID" != "$EXPECTED_SSID"); then
    echo "Restarting Wi-Fi"
    networksetup -setairportpower "${INTERFACE}" off
    networksetup -setairportpower "${INTERFACE}" on
fi
