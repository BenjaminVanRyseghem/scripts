#!/bin/bash

set -e

SCRIPT_NAME='run-with-flags.sh'

APP=$1
FLAGS=${@:2}

# Create a backup file to be read from later on
if [ ! -f "${APP}/Contents/Info.plist.bak" ]; then
    cp "${APP}/Contents/Info.plist" "${APP}/Contents/Info.plist.bak"
fi  

EXECUTABLE=`sed -n '/CFBundleExecutable/{n;p;}' "${APP}/Contents/Info.plist.bak" | sed -E 's/[ 	]*<\/?string>//g'`

# Generate script with flags
cat << EOF > "${APP}/Contents/MacOS/${SCRIPT_NAME}"
#!/bin/bash
cd "\$(dirname "\$0")"
exec "./${EXECUTABLE}" "${FLAGS}"
EOF

# Make script executable
chmod +x "${APP}/Contents/MacOS/${SCRIPT_NAME}"

# Replace the old name by the new one in Info.plist
CONTENT=`sed "/CFBundleExecutable/{
    t
    n
    s/${EXECUTABLE}/${SCRIPT_NAME}/
}" "${APP}/Contents/Info.plist.bak"`

echo "${CONTENT}" > "${APP}/Contents/Info.plist"

# Refresh Info.plist cache
TMP_FOLDER=`mktemp -d`
TMP_NAME=$(uuidgen)
mv "${APP}" "${TMP_FOLDER}/${TMP_NAME}"
mv "${TMP_FOLDER}/${TMP_NAME}" "${APP}"