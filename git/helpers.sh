#!/bin/sh

function get_sha() {
  commit=$(git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr)%Creset %G? %C(bold blue)<%an>%Creset' --abbrev-commit -n $1 | fzf --reverse --ansi)

  if [ -z "$commit" ]; then
    exit 1;
  fi

  sha=$(echo "$commit" | sed -e 's/^\* //' -e 's/ -.*$//')

  echo $sha
}