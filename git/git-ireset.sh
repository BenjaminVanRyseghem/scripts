#!/bin/sh
set -e

source "$(dirname $0)/helpers.sh"

new_args=$(echo "$@" | sed -Ee 's/^-n [0-9]+//' -e 's/ -n [0-9]+//')

NUMBER_OF_COMMITS=200

while getopts ":n:" opt; do
  case "$opt" in
    n)
      NUMBER_OF_COMMITS=$OPTARG
      ;;
    ?)
      ;;
  esac
done

sha=$(get_sha $NUMBER_OF_COMMITS)

git reset $new_args "$sha"~