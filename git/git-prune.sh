#!/bin/sh
git fetch -p > /dev/null 2>&1
branches=`git branch -vv | grep ': gone]' | awk '{print $1}'`

if [[ -z "$branches" ]]
then
    echo "No branch to prune"
    exit 0
fi

echo "You are about to delete those branches:"
for branch in $branches; do echo $branch; done
echo
read -p "Are you sure? " -n 1 -r
echo
if [[ $REPLY =~ ^[Yy]$ ]]
then
    for branch in $branches; do git branch -D $branch; done
fi