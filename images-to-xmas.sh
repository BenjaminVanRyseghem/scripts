#!/bin/bash

#CONSTANTS

VERSION=1.0.0
SIZE=142.0

#DEFAULTS

OUTPUT="final.png"
FONT=helvetica
FONT_FILL="white"
FONT_STROKE="black"
FONT_OFFSET=0

showVersion() {
    echo $VERSION;
    exit 0;
}

showHelp() {
    echo "Version: $VERSION
Copyright: © 2020 Benjamin Van Ryseghem <benjamin@vanryseghem.com>

Use imagemagick (convert) to convert 24 images into a page  of Xmas tree badges.

Options:
    --output        -o      Output file  name. \`output.png\` by default
    --font          -f      Font name or path. See convert \`-font\` argument
    --font-fill     -F      Font fill color. See convert \`-fill\` argument
    --font-stroke   -s      Font stroke color. See convert \`-stroke\` argument
    --font-offset   -O      Font offset. See convert \`-annotate\` argument
    --help          -h      Show this help message
    --version       -v      Show the version

"; 
    exit 0;
}

while [[ "$#" -gt 0 ]]; do
    case $1 in
        -v|--version) showVersion ;;
        -h|--help) showHelp ;;
        -o|--output) OUTPUT=$2; shift ;;
        -f|--font) FONT=$2; shift ;;
        -F|--font-fill) FONT_FILL=$2; shift ;;
        -s|--font-stroke) FONT_STROKE=$2; shift ;;
        -O|--font-offset) FONT_OFFSET=$2; shift ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done



TMP_DIR=$(mktemp -d 2>/dev/null || mktemp -d -t 'TMP_DIR')

REDUCE_FACTOR=`bc -l <<< '100/142'`
REDUCED=`bc -l <<< "$SIZE * $REDUCE_FACTOR" | awk '{print int($1)}'`

INDEX=1
files=`ls *.png`
for i in $files; do 
    convert \
        $i \
        -background none \
        -gravity center \
        -resize ${REDUCED}x${REDUCED} \
        -extent ${SIZE}x${SIZE} \
        - | \
    convert  - \
        -fill transparent \
        -stroke black \
        -draw "circle 70,70 0,70" \
        - | \
    convert - \
        -font "$FONT" \
        -fill "$FONT_FILL" \
        -stroke "$FONT_STROKE" \
        -gravity South \
        -pointsize 30 \
        -annotate +0+$((15 + $FONT_OFFSET)) $INDEX \
        "$TMP_DIR/${INDEX}.png"
    let INDEX=${INDEX}+1
done

for i in 0 1 2 3 4 5; do
    convert  "$TMP_DIR/$(($i * 4 + 1)).png" "$TMP_DIR/$(($i * 4 + 2)).png" "$TMP_DIR/$(($i * 4 + 3)).png" "$TMP_DIR/$(($i * 4 + 4)).png" -background none +smush 4 -resize 2480x3508 "$TMP_DIR/page_0$(($i + 1)).png"
done

convert "$TMP_DIR/page_01.png" "$TMP_DIR/page_02.png" "$TMP_DIR/page_03.png" "$TMP_DIR/page_04.png" "$TMP_DIR/page_05.png" "$TMP_DIR/page_06.png" -background none -smush 4 -resize 2480x3508 "$OUTPUT"
rm -rf "$TMP_DIR"