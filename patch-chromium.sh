#!/bin/sh

set -e

cd "$(dirname "$0")"

./add-flags-to-app.sh "/Applications/Chromium.app" --remote-debugging-port=9222